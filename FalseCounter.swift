import SwiftUI

// Race Condition

class Counter {
    var value = 0
    var values:[Int] = []
    
    func qincrement() -> [Int]{
        self.value += 1
        values.append(value)
        return values
    }
    
    func sincrement() -> [Int]{
//        print("sinc begin")
        let newValue = self.value
        let rnd = Double.random(in: 0.5...1.5)
        DispatchQueue.main.asyncAfter(deadline: .now() + rnd) {
            self.value = newValue + 1
//            print("sinc end")
            self.values.append(self.value)
        }
        return self.values
    }
}

var colorDict:[Int:Color] = [:]

struct ContentView: View {
    var counter = Counter()
    @State var refresh = 0
    @State var offset:CGSize = .zero
    @State var bars:[Int] = []
    
    
    let timer1 = Timer.publish(every: 0.2, on: .main, in: .common).autoconnect()
    
    var body: some View {
        VStack {
            Color.clear
                .frame(width: 1, height: 1, alignment: .center)
                .onReceive(timer1) { _ in
                    DispatchQueue.global(qos: .userInitiated).async {
                        bars = counter.qincrement()
                    }
                }
                .onReceive(timer1) { _ in
                    DispatchQueue.global(qos: .userInitiated).async {
                        bars = counter.sincrement()
                    }
                }
                .onReceive(timer1) { _ in
                    print("refresh ",refresh)
                    if refresh > 15 {
                        offset.width -= 24
                    }
                    refresh += 1
                }
                .id(refresh)
            BarView(bars: $bars, multiple: 4)
                .offset(offset)
        }
    }
}

struct BarView: View {
    let colors = [Color.red, .purple, .yellow, .pink, .green, .teal, .orange, .indigo, .blue]
    @Binding var bars:[Int]
    @State var multiple:Int
    
    var body: some View {
        HStack {
                ForEach((0..<bars.count), id: \.self) { count in
                    VStack {
                        Text(String(bars[count]))
                            .font(.caption).bold()
                            .foregroundColor(Color.gray)
                            .frame(width: 17, alignment: .center)
                        Rectangle()
                            .fill(returnColor(barSize: bars[count]))
                            .frame(width: 18, height: CGFloat(bars[count] * multiple), alignment: .center)
                    }
                }
        }.frame(width: screenSize.width - 64, alignment: .leading)
        
    }
    func returnColor(barSize:Int) -> Color {
        if colorDict[barSize] != nil {
            return colorDict[barSize]!
        } else {
            let colorIndex = barSize % colors.count
            colorDict[barSize] = colors[colorIndex]
            return colors[colorIndex]
        }
    }
}