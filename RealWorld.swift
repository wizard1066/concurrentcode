import SwiftUI
import CoreImage.CIFilterBuiltins

struct ContentView: View {
  @StateObject var model = CaptureModel()
  @State var swapView = false
  @State var filterImage: Image!
  var body: some View {
    VStack {
      if !swapView {
        ZStack {
          model.capturedImage.map { capturedImage in
            Image(uiImage: capturedImage)
              .resizable()
              .scaledToFit()
            //                      .task {
            //              let filters = CIFilter.filterNames(inCategories: nil)
            //              for filtername in filters {
            //               if let attrib = CIFilter.init(name: filtername)?.attributes {
            //                 print("filtername \(filtername)")
            ////                 print("attributes \(attrib)")
            //               }
            //              }
            //            }
          }
        }
      } else {
        VStack {
          Spacer()
          filterImage
            .resizable()
            .scaledToFit()
            .rotationEffect(.degrees(90))
            .padding()
          Spacer()
        }
      }
      Spacer()
      Text("Take a Photo")
        .padding()
        .onTapGesture {
          if UIImagePickerController.isSourceTypeAvailable(.camera) {
            model.startRunningCaptureSession()
          }
        }
        .onLongPressGesture {
          print("processing")
          let imageUI = model.capturedImage!
          print("size ",imageUI.size)
//          let foo = anaImage(image: imageUI)
          
          let foo = anaImage(image: UIImage(named: "IMG_2133-1")!)
          
//          print(model.capturedImage?.size, foo.rgbColor.count)
//          let imageCI = CIImage(image: imageUI)
          let imageCI = CIImage(image: UIImage(named: "IMG_2133-1")!)
          if imageCI != nil {
//            let newImage = doFilter(imageCI!,intensity:0.9)
//            if newImage != nil {
//              print("processing II")
//              let foo2 = anaImage(image: newImage!)
//              print(newImage?.size, foo2.rgbColor.count)
//              filterImage = Image(uiImage: newImage!)
//              //                      images.append(newImage!)
//            }
            //
          } else {
            print("failed ")
          }
        }
      Text("Show Photos")
        .padding()
        .onTapGesture {
          swapView.toggle()
        }
    }
    .onDisappear {
      model.stopRunningCaptureSession()
    }
  }
  
  
  
  func doFilter(_ input: CIImage, intensity: Double) -> UIImage?
  {
    let context = CIContext()
    
    // *** CRASHES ***
    //      let effectNode = CIFilter.circleSplashDistortion()
    //      effectNode.setValue(input, forKey: "inputImage")
    //      effectNode.setValue(CIVector(x: input.extent.size.width / 2, y: input.extent.size.height / 2), forKey: "inputCenter")
    //      effectNode.setValue(150.0, forKey: "inputRadius")
    
    
    //      let effectNode = CIFilter.bumpDistortionLinear()
    //      effectNode.setValue(input, forKey: "inputImage")
    //      effectNode.setValue(CIVector(x: input.extent.size.width / 2, y: input.extent.size.height / 2), forKey: "inputCenter")
    //      effectNode.setValue(1024, forKey: "inputRadius")
    //      effectNode.setValue(0.5, forKey: "inputScale")
    //      effectNode.setValue(90.0, forKey: "inputAngle")
    
    
    //      let effectNode = CIFilter.bumpDistortion()
    //      effectNode.setValue(input, forKey: "inputImage")
    //       effectNode.setValue(CIVector(x: input.extent.size.width / 2, y: input.extent.size.height / 2), forKey: "inputCenter")
    //        effectNode.setValue(1024, forKey: "inputRadius")
    //        effectNode.setValue(0.5, forKey: "inputScale")
    
    // Makes Darker 1 nothing
    //      var effectNode = CIFilter.vignetteEffect()
    //        effectNode.setValue(input, forKey: "inputImage")
    //        effectNode.setValue(128, forKey: "inputRadius")
    //        effectNode.setValue(0.5, forKey: "inputIntensity")
    
    //        let effectNode = CIFilter.photoEffectProcess()
    //        effectNode.setValue(input, forKey: "inputImage")
    
    //        let effectNode = CIFilter.photoEffectMono()
    //        effectNode.setValue(input, forKey: "inputImage")
    
    //        let effectNode = CIFilter.photoEffectInstant()
    //        effectNode.setValue(input, forKey: "inputImage")
    //
    //        let effectNode = CIFilter.colorMonochrome()
    //        effectNode.setValue(input, forKey: "inputImage")
    
    //      let effectNode = CIFilter.colorInvert()
    //      effectNode.setValue(input, forKey: "inputImage")
    
    //        let effectNode = CIFilter.colorPosterize()
    //        effectNode.setValue(input, forKey: "inputImage")
    //        effectNode.setValue(8, forKey: "inputLevels")
    
    //        let effectNode = CIFilter.sepiaTone()
    //        effectNode.setValue(input, forKey: "inputImage")
    
    //      let effectNode = CIFilter.pixellate()
    //      effectNode.setValue(input, forKey: "inputImage")
    //      effectNode.setValue(64, forKey: "inputScale")
    
          
    
//          let effectNode = CIFilter.comicEffect()
//          effectNode.setValue(input, forKey: "inputImage")
          
          
    
//     Only works if you use a CGRect
    let effectNode = CIFilter.linearGradient()
    effectNode.setValue(CIColor.blue, forKey: "inputColor0")
    effectNode.setValue(CIColor.blue, forKey: "inputColor1")
    effectNode.setValue(CIVector(x: 0, y: 0), forKey: "inputPoint0")
    effectNode.setValue(CIVector(x: 2320, y: 3088), forKey: "inputPoint1")

    guard let outputImage = effectNode.outputImage else { return UIImage() }
    //
    if let cgimg = context.createCGImage(outputImage, from: CGRect(x: 0, y: 0, width: 2320, height: 3088)) {
      let filteredImage = UIImage(cgImage: cgimg)
      return filteredImage
    }
    return UIImage()
    
    
    
//          if let outputImage = effectNode.outputImage {
//              if let cgimg = context.createCGImage(outputImage, from: outputImage.extent) {
//                  return UIImage(cgImage: cgimg)
//              }
//          }
//
//    return nil
  }
  
  struct Ana {
    var rgbRed:[UInt32?:UInt64]
    var rgbBlue:[UInt32?:UInt64]
    var rgbGreen:[UInt32?:UInt64]
    var rgbColor:[UInt32?:UInt64]
  }
  
  // try with Sets + try different loop
  
  struct Qna {
    var rgbRed:Set<UInt32> = []
    var rgbBlue:Set<UInt32> = []
    var rgbGreen:Set<UInt32> = []
    var rgbColor:Set<UInt32> = []
    
    mutating func countQna(p:UInt32) {
      rgbColor.insert(p)
    }
    
    func confirmMembers() -> Int {
      return rgbColor.count
    }
  }
  
  actor Sna {
    var rgbRed:Set<UInt32> = []
    var rgbBlue:Set<UInt32> = []
    var rgbGreen:Set<UInt32> = []
    var rgbColor:Set<UInt32> = []
    
    func countSna(p:UInt32) async {
      rgbColor.insert(p)
    }
    
    func verifyRed(p:UInt32) async {
      let redCheck = p & 0xff0000ff
      rgbRed.insert(redCheck)
    }
    
    func verifyBlue(p:UInt32) async {
      let redCheck = p & 0xff00ff00
      rgbRed.insert(redCheck)
    }
    
    func verifyGreen(p:UInt32) async {
      let redCheck = p & 0xffff0000
      rgbRed.insert(redCheck)
    }
    
    func confirmMembers() -> Int {
      return rgbColor.count
    }
  }
  
  actor Pna {
    var rgbRed:[UInt32?:Int] = [:]
    var rgbBlue:[UInt32?:Int] = [:]
    var rgbGreen:[UInt32?:Int] = [:]
    var rgbColor:[UInt32?:Int] = [:]
    
    func proPna(p:UInt32) async {
      if await countPna(p:p) == nil {
         setPna(p:p)
      } else {
         incPna(p:p)
      }
    }
    
    func countPna(p:UInt32)  -> Int? {
      return rgbColor[p]
    }
    
    func setPna(p:UInt32) {
      rgbColor[p] = 1
    }
    
    func incPna(p:UInt32) {
      rgbColor[p] = rgbColor[p]! + 1
    }
    
    func outPna() -> Int {
      return(rgbColor.count)
    }
  }
  
  fileprivate func anaImage(image: UIImage) -> (Ana) {
    let height = Int((image.size.height))
    let width = Int((image.size.width))
    var ana = Ana(rgbRed: [nil: 0], rgbBlue: [nil:0], rgbGreen: [nil:0], rgbColor: [nil:0])
    
    
    let bitsPerComponent = Int(8)
    let bytesPerRow = 4 * width
    let colorSpace = CGColorSpaceCreateDeviceRGB()
    let rawData = UnsafeMutablePointer<UInt32>.allocate(capacity: (width * height))
    
    let bitmapInfo: UInt32 = CGBitmapInfo.byteOrder32Big.rawValue | CGImageAlphaInfo.premultipliedLast.rawValue
    let rect = CGRect(origin: CGPoint.zero, size: (image.size))
    
    
    let imageContext = CGContext(data: rawData, width: width, height: height, bitsPerComponent: bitsPerComponent, bytesPerRow: bytesPerRow, space: colorSpace, bitmapInfo: bitmapInfo)
    
    imageContext?.draw(image.cgImage!, in: rect)
    
    let pixels = UnsafeMutableBufferPointer<UInt32>(start: rawData, count: width * height)
    
//    print("pixels ",pixels.count,pixels.baseAddress,pixels[pixels.count / 2])

    let pixSlice = pixels.count / 6
    
    
    let pix1 = pixels[(0..<pixels.count / 2)]
    let pix2 = pixels[(pixels.count / 2)...]
    
    let begin = CFAbsoluteTimeGetCurrent()
//    for p in pixels.indices {
//      if ana.rgbColor[pixels[p]] == nil {
//        ana.rgbColor[pixels[p]] = 1
//      } else {
//        ana.rgbColor[pixels[p]] = ana.rgbColor[pixels[p]]! + 1
//      }
//    }
//    let _ = pixels.indices.map { value in if ana.rgbColor[pixels[value]] == nil {
//        ana.rgbColor[pixels[value]] = 1
//      } else {
//        ana.rgbColor[pixels[value]] = ana.rgbColor[pixels[value]]! + 1
//      }}
//    print("timeTaken ",CFAbsoluteTimeGetCurrent() - begin)
//    print("ana ",ana.rgbColor.count)
//    print("pix ",pixels.indices, pixels.count / 2)
    
//
    
  
    
    @Sendable func t1() async -> Sna {
      var pna = Sna()
    
      for value in pix1.indices {
        await pna.countSna(p: pixels[value])
      }
        let foo = await pna.confirmMembers()
        print("foo \(foo)")
      return pna
//      print("end \(CFAbsoluteTimeGetCurrent())")
    }

//    let t2 = Task.detached(priority: .userInitiated) {
//      print("begin \(CFAbsoluteTimeGetCurrent())")
//
//
    @Sendable func t2() async -> Sna {
        var qna = Sna()
        for value in pix2.indices {
          await qna.countSna(p: pixels[value])
        }
        let foo = await qna.confirmMembers()
        print("foo \(foo)")
        return qna
      }
        
    Task.detached(priority: .userInitiated) {
      let cores = 5
      let newAna = await withTaskGroup(of: Sna.self) { group -> [Sna] in
//        group.addTask { await t1() }
//        group.addTask { await t2() }
        group.addTask { await doAna(pixels: pixels, begin: 0, end: pixels.count / cores) }
        group.addTask { await doAna(pixels: pixels, begin: pixels.count / cores, end: pixels.count / cores * 2) }
        group.addTask { await doAna(pixels: pixels, begin: pixels.count / cores * 2, end: pixels.count) }
        group.addTask { await doAna(pixels: pixels, begin: pixels.count / cores * 3, end: pixels.count) }
        group.addTask { await doAna(pixels: pixels, begin: pixels.count / cores * 4, end: pixels.count) }
        
        var collected = [Sna]()
        if !group.isCancelled {
          for await value in group {
            collected.append(value)
          }
        }
        return collected
      }
      print("collected \(await newAna.first?.rgbColor.count)")
      print("collected2 \(await newAna.last?.rgbColor.count)")
      let foo = await newAna[0].rgbColor.union(await newAna[1].rgbColor)
      let foo2 = foo.union(await newAna[2].rgbColor)
      let foo3 = foo.union(await newAna[3].rgbColor)
      let foo4 = foo.union(await newAna[3].rgbColor)
      print("answer ",foo4.count)
      print("timeTaken ",CFAbsoluteTimeGetCurrent() - begin)
     }
      
      
//      let newColours = try await withThrowingTaskGroup(of: Color.self) { group -> [Color] in
//        group.addTask { try await changer(newColor:.green, delay: Int.random(in: 0...7)) }
//        group.addTask { try await changer(newColor:.blue, delay: Int.random(in: 0...7)) }
//        group.addTask { try await changer(newColor:.red, delay: Int.random(in: 0...7)) }
//        //        group.cancelAll()
//        var collected = [Color]()
//        if !group.isCancelled {
//          for try await value in group {
//            collected.append(value)
//          }
//        }
//        return collected
//      }
//      return newColours
//  }
        
//      let sna = Sna()
//
//        for value in 0..<pixels.count {
//          await sna.countSna(p: pixels[value])
//        }
//
//        let foo = await sna.confirmMembers()
//        print("pna \(foo)")
      
//        for value in pixels.indices {
//          await sna.countSna(p: pixels[value])
//        }
//
//        let foo = await sna.confirmMembers()
//        print("pna \(foo)")
        
//      let pna = Pna()
////      for value in pixels.indices {
////        await pna.proPna(p: pixels[value])
////      }
//      let _ = await pixels.indices.asyncMap( { value in await pna.proPna(p: pixels[value]) } )
//      let foo = await pna.outPna()
//      print("pna \(foo)")
//
//      print("timeTaken ",CFAbsoluteTimeGetCurrent() - begin)
////      await print("ana ",pna.rgbColor.count)
//    }
    
//    let outContext = CGContext(data: pixels.baseAddress, width: width, height: height, bitsPerComponent: bitsPerComponent,bytesPerRow: bytesPerRow,space: colorSpace,bitmapInfo: bitmapInfo,releaseCallback: nil,releaseInfo: nil)
//
//    let outImage = UIImage(cgImage: outContext!.makeImage()!)
    return (ana)
    
    
  }
  
  @Sendable func doAna(pixels:UnsafeMutableBufferPointer<UInt32>, begin:Int, end:Int) async -> Sna {
      let pna = Sna()
    
      let pix = pixels[(begin..<end)]
      for value in pix.indices {
        await pna.countSna(p: pixels[value])
      }
        let foo = await pna.confirmMembers()
        print("foo \(foo)")
      return pna
//      print("end \(CFAbsoluteTimeGetCurrent())")
    }
  
  fileprivate func inverseImage(pixels:UnsafeMutableBufferPointer<UInt32>) -> UnsafeMutableBufferPointer<UInt32>
  {
    for p in pixels.indices {
      pixels[p] = ~pixels[p] // Bitwise NOT [option n]
      pixels[p] = pixels[p] | 0xff000000 // Bitwise OR
    }
    return pixels
  }
  
}//struct

extension Sequence {
    func asyncMap<T>(
        _ transform: (Element) async throws -> T
    ) async rethrows -> [T] {
        var values = [T]()

        for element in self {
            try await values.append(transform(element))
        }

        return values
    }
}


//struct ContentViewX: View {
//  @State var qrCode:Image!
//  @State var isReady = false
//  var body: some View {
//    Text("QRCode")
//      .padding()
//      .onTapGesture {
//        let id = "vsantos@shms.com"
//        let data = Data(id.utf8)
//        qrCode = doFilter(data: data)
//        isReady = true
//      }
//      .task {
//        let filters = CIFilter.filterNames(inCategories: nil)
//        for filtername in filters {
//          if let attrib = CIFilter.init(name: filtername)?.attributes {
//            print("filtername \(filtername)")
//            //                 print("attributes \(attrib)")
//          }
//        }
//      }
//    if isReady {
//      qrCode
//        .interpolation(.none)
//        .resizable()
//        .frame(width: 256, height: 256)
//        .frame(maxWidth: .infinity)
//    }
//  }
//
//
//  func doFilter(data: Data) -> Image {
//    let context = CIContext()
//    let filter = CIFilter.qrCodeGenerator()
//
//    filter.setValue(data, forKey: "inputMessage")
//
//    if let outputImage = filter.outputImage {
//      if let cgimg = context.createCGImage(outputImage, from: outputImage.extent) {
//        return Image(uiImage: UIImage(cgImage: cgimg))
//      }
//    }
//    return Image(uiImage: UIImage(systemName: "xmark.circle") ?? UIImage())
//  }
//}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
