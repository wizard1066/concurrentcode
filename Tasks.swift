import SwiftUI

struct ContentView: View {
  @State var colour:[Color] = Array(repeating: Color.clear, count: 4)
  @State var textDelay:[Color:Int] = [Color.green:0,Color.blue:0,Color.red:0]
  @State var dColours = [Color.red,Color.green,Color.blue]
  
  var body: some View {
    ZStack {
      ForEach ((0...2), id: \.self) { indix in
        //          Text(String(indix + 1))
        Text(String(textDelay[dColours[indix]]!))
          .font(.largeTitle).bold()
          .offset(returnOffset(indix: indix))
          .blendMode(.screen)
        Circle()
          .fill(colour[indix])
          .overlay(Circle()
                    .stroke(Color.white))
          .frame(width: 128, height: 128, alignment: .center)
          .animation(.linear(duration: 4), value: colour[indix])
          .offset(returnOffset(indix: indix))
          .blendMode(.screen)
//          .task {
//            do {
//              try await serialTasks()
//            } catch {
//              print(error.localizedDescription)
//            }
//          }
      }
    }.task {
      do {
        //              try await parrallelTasksI()
        //                try await parrallelTasksII()
        //                try await parrallelTasksIII()
        //                try await parrallelTasksIV()
//                        try await parrallelTasksV()
//        try colour = await newGroup()
//          try colour = await newGroupV()
          let colours = try await mixedGroup()
          
          colour = colours.map { $0.color }
          
          
      } catch {
        print(error.localizedDescription)
      }
    }
  }
  
  func returnOffset(indix:Int) -> CGSize {
    switch indix {
    case 0:
      return CGSize(width: 0, height: -16)
    case 1:
      return CGSize(width: -48, height: +64)
    case 2:
      return CGSize(width: +48, height: +64)
    default:
      assert(true, "Never Happens")
    }
    return CGSize.zero
  }
  
  
  func serialTasks() async throws {
    // Runs serial
    try await colour[0] = changer(newColor:.green, delay: 2)
    try await colour[1] = changer(newColor:.blue, delay: 2)
    try await colour[2] = changer(newColor:.red, delay: 2)
  }
  
  func parrallelTasksI() async throws {
    async let colour1 = changer(newColor:.green, delay: Int.random(in: 0...7))
    async let colour2 = changer(newColor:.blue, delay: Int.random(in: 0...7))
    async let colour3 = changer(newColor:.red, delay: Int.random(in: 0...7))
    
    try await colour[0] = colour1
    try await colour[1] = colour2
    try await colour[2] = colour3
  }
  
  func parrallelTasksII() async throws {

    Task {
      async let colour1 = changer(newColor:.green, delay: Int.random(in: 0...7))
      colour[0] = try await colour1
    }
    Task {
      async let colour2 = changer(newColor:.blue, delay: Int.random(in: 0...7))
      colour[1] = try await colour2
    }
    Task {
      async let colour3 = changer(newColor:.red, delay: Int.random(in: 0...7))
      colour[2] = try await colour3
    }
  }
  
  func parrallelTasksIII() async throws {
    let greenTask = Task {
        try await changer(newColor:.green, delay:Int.random(in: 0...7))
    }
    
    let blueTask = Task {
      try await changer(newColor:.blue, delay:Int.random(in: 0...7))
    }
    
    let redTask = Task {
      try await changer(newColor:.red, delay:Int.random(in: 0...7))
    }
    blueTask.cancel()
    Task {
      if !greenTask.isCancelled {
        colour[0] = try await greenTask.value
      }
    }
    Task {
      if !blueTask.isCancelled {
        colour[1] = try await blueTask.value
      }
    }
    Task {
      if !redTask.isCancelled {
        colour[2] = try await redTask.value
      }
    }
  }
  
  func parrallelTasksIV() async throws {
    Task {
      let greenColor = Task {
        try await changer(newColor:.green, delay:Int.random(in: 0...7))
      }
      if !Task.isCancelled {
        let c0 = await greenColor.result
        colour[0] = try c0.get()
      }
    }
    Task {
      let blueColor = Task {
        try await changer(newColor:.blue, delay:Int.random(in: 0...7))
      }
      if !Task.isCancelled {
        let c1 = await blueColor.result
        colour[1] = try c1.get()
      }
    }
    Task {
      let redColor = Task {
        try await changer(newColor:.red, delay:Int.random(in: 0...7))
//        await Task.yield()
      }
      if !Task.isCancelled {
        let c2 = await redColor.result
        colour[2] = try c2.get()
      }
    }
  }
  
  func parrallelTasksV() async throws {
    Task.detached(priority: .low) {
      let greenColor = Task {
        try await changer(newColor:.red, delay:Int.random(in: 0...7))
      }
      if !greenColor.isCancelled {
        await setColor(indix: 0, result: await greenColor.result)
      }
    }
    Task.detached(priority: .low) {
      let blueColor = Task {
        try await changer(newColor:.green, delay:Int.random(in: 0...7))
      }
      if !blueColor.isCancelled {
        await setColor(indix: 1, result: await blueColor.result)
      }
    }
    Task.detached(priority: .low) {
      let redColor = Task {
        try await changer(newColor:.blue, delay:Int.random(in: 0...7))
        // await Task.yield()
      }
      if !redColor.isCancelled {
        await setColor(indix: 2, result: await redColor.result)
      }
    }
  }
  
  @MainActor
  func setColor(indix:Int, result:Result<Color, Error>) {
    switch result {
    case .success(let value):
      colour[indix] = value
    case .failure(let error):
      print(error.localizedDescription)
    }
  }
  
  func newGroup() async throws -> [Color] {
      let newColours = try await withThrowingTaskGroup(of: Color.self) { group -> [Color] in
        group.addTask { try await changer(newColor:.green, delay: Int.random(in: 0...7)) }
        group.addTask { try await changer(newColor:.blue, delay: Int.random(in: 0...7)) }
        group.addTask { try await changer(newColor:.red, delay: Int.random(in: 0...7)) }
        //        group.cancelAll()
        var collected = [Color]()
        if !group.isCancelled {
          for try await value in group {
            collected.append(value)
          }
        }
        return collected
      }
      return newColours
  }
  
  func newGroupV() async throws -> [Color] {
    // parrallel tasks
      let newColours = try await withThrowingTaskGroup(of: Color.self) { group -> [Color] in
        var collected = [Color.clear, Color.clear]
        
        let task1 = group.addTaskUnlessCancelled(priority: .low)  { try await changer(newColor:.green, delay: 4) }
        if task1 {
          // extract the value before group completes
          let preView = try await group.next()
          print("low priority task1 completed \(preView!)")
          collected[0] = preView!
        }
        let task2 = group.addTaskUnlessCancelled(priority: .medium)  { try await changer(newColor:.blue, delay: 2) }
        if task2 {
          let exitView = try await group.nextResult()
          print("medium priority task2 completed \(exitView)")
          switch exitView {
            case .success(let value):
              collected[1] = value
              colour[1] = value
            case .failure(let error):
              print(error.localizedDescription)
            case .none:
              assert(true,"Should Never Happen")
            }
          // extract the result [includes value] before group completes
        }
        let task3 = group.addTaskUnlessCancelled(priority: .high) {
          try await changer(newColor:.red, delay: 8)
        }
        if task3 {
          print("high priority task3 completed")
        }
        
//        group.cancelAll()
        
        if !group.isCancelled {
          for try await value in group {
            collected.append(value)
            print("collected \(value)")
          }
        }
//        let newCollection = [Color.clear, Color.clear, Color.clear]
//        let zippedCollection = [collected,newCollection].reduce([],+)
        return collected
      }
    return newColours
  }
  
  
  struct MixedType {
    var value: Int!
    var color: Color
  }
  
  var values:[MixedType] = []
  
  func mixedGroup() async throws -> [MixedType]{
    // parrallel
    let values = try await withThrowingTaskGroup(of: MixedType.self) { group -> [MixedType] in
//      group.addTask { MixedType(value: 7, color: Color.green) }
//      group.addTask { MixedType(value: 3, color: Color.blue) }
//      group.addTask { MixedType(value: 5, color: Color.red) }
      group.addTask { try await changerV(newValues: MixedType(value: 5, color: Color.green)) }
      group.addTask { try await changerV(newValues: MixedType(value: 7, color: Color.blue)) }
      group.addTask { try await changerV(newValues: MixedType(value: 3, color: Color.red)) }
      var collected = [MixedType]()
      for try await value in group {
        collected.append(value)
      }
      return collected
    }
    return values
  }
  
  func changerV(newValues:MixedType) async throws -> MixedType {
    debugPrint("delay \(newValues.color) \(newValues.value)")
    
    try await Task.sleep(seconds: newValues.value)
    textDelay[newValues.color] = newValues.value
    return newValues
  }
  
  func changer(newColor:Color, delay:Int) async throws -> Color {
    debugPrint("delay \(newColor) \(delay)")
    
    try await Task.sleep(seconds: delay)
    textDelay[newColor] = delay
    return newColor
  }
}

extension Task where Success == Never, Failure == Never {
  static func sleep(seconds: Int) async throws {
    let duration = UInt64(seconds * 1_000_000_000)
    try await sleep(nanoseconds: duration)
  }
}