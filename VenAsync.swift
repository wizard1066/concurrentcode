import SwiftUI
import CoreMotion

struct Motions {
  var dx: Double!
  var dy: Double!
  var dz: Double!
}

actor MotionManager: AsyncIteratorProtocol, AsyncSequence {
  
  typealias Element = Motions
  
  func next() async -> Element? {
    return self.motion
  }
  
  nonisolated func makeAsyncIterator() -> MotionManager {
      self
  }
  
  private let motionManager = CMMotionManager()
  var motion:Motions = Motions(dx: 0, dy: 0, dz: 0)
  
  func setup() {
    motionManager.startDeviceMotionUpdates(to: .main) { data, error in
        guard let newData = data?.gravity else { return }
        self.motion = Motions(dx: newData.x, dy: newData.y, dz: newData.z)
  }
  
  func shutdown() {
    motionManager.stopDeviceMotionUpdates()
  }

  }
}

struct ContentView: View {
  @State var motion = MotionManager()
  @State var yValue = 0.0
  var body: some View {
      
    CirclesView(yMan: $yValue)
          .frame(maxWidth: .infinity, maxHeight: .infinity)
          .background(Color.black)
          .edgesIgnoringSafeArea(.all)
          .onAppear {
            Task {
              await doMotion()
            }
          }
    
  }
  
  @MainActor
  func doMotion() async {
    await motion.setup()
    Timer.scheduledTimer(withTimeInterval: 0.02, repeats: true) { _ in
      Task.detached(priority: .userInitiated) {
        
        for await value in motion {
          yValue = value.dy
        }
      }
    }
  }
}

struct CirclesView: View {
  @Binding var yMan: Double
  var body: some View {
    ZStack {
      Circle()
        .fill(Color.red)
        .frame(width: 256, height: 256)
        .offset(x: 0, y: yMan * 200)
        .blendMode(.screen)

      Circle()
        .fill(Color.green)
        .frame(width: 256, height: 256)
        .offset(x: yMan * 200, y: yMan * -200)
        .blendMode(.screen)

      Circle()
        .fill(Color.blue)
        .frame(width: 256, height: 256)
        .offset(x: yMan * -200, y: yMan * -200)
        .blendMode(.screen)
    }.id(yMan)
  }
}