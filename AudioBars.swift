import SwiftUI
import AVFoundation
import CoreAudio

var recorder: AVAudioRecorder!
  var levelTimer = Timer()
  var lowPassResults: Double = 0.0

class AudioMan:AsyncSequence, AsyncIteratorProtocol {
  
  typealias Element = Float
  
  func next() async -> Element? {
    recorder.updateMeters()
    
    if recorder.averagePower(forChannel: 0) < -4 {
      return nil
    } else {
      return recorder.averagePower(forChannel: 0)
    }
  }
  
  func makeAsyncIterator() -> AudioMan {
    self
  }
  
  func setup() {
    //make an AudioSession, set it to PlayAndRecord and make it active
    let audioSession:AVAudioSession = AVAudioSession.sharedInstance()
    try! audioSession.setCategory(AVAudioSession.Category.playAndRecord)
    
    try! audioSession.setActive(true, options: .notifyOthersOnDeactivation)
    
    //set up the URL for the audio file
    let documents = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0])
    let url = documents.appendingPathComponent("record.caf")
    
    let recordSettings: [String: Any] = [
      AVFormatIDKey:              kAudioFormatAppleIMA4,
      AVSampleRateKey:            44100.0,
      AVNumberOfChannelsKey:      2,
      AVEncoderBitRateKey:        12800,
      AVLinearPCMBitDepthKey:     16,
      AVEncoderAudioQualityKey:   AVAudioQuality.max.rawValue]
    
    
    //Instantiate an AVAudioRecorder
    recorder = try! AVAudioRecorder(url:url, settings: recordSettings)
    
    recorder.prepareToRecord()
    recorder.isMeteringEnabled = true
    
    //start recording
    
    recorder.record()
  }
}

struct AudioBars:Hashable {
  var id = UUID()
  var value: CGFloat!
}

struct ContentView: View {
  @State var viewType = 0
  @State var micAudio = AudioMan()
  @State var chart:[AudioBars] = []
  @State var previousValue: CGFloat = 0
  var body: some View {
    Text("Listening World")
      .onAppear {
        Task {
          await doAudio()
        }
      }
      HStack(alignment: .center, spacing: 0) {
      ForEach (chart, id: \.self) { high in
        Rectangle()
          .fill(LinearGradient(gradient: Gradient(colors: [.blue, .green]), startPoint: .top, endPoint: .bottom))
          .frame(width: 4, height: high.value, alignment: .center)
      }
    }
  }
  
  @MainActor
  func doAudio() async {
    micAudio.setup()
    Timer.scheduledTimer(withTimeInterval: 0.02, repeats: true) { _ in
      Task.detached(priority: .userInitiated) {
          for await number in micAudio {
            if number != AudioMan.Element(previousValue) {
              let newNode = AudioBars(id: UUID(), value: abs(CGFloat(number) * 10))
              chart.append(newNode )
              previousValue = CGFloat(number)
            }
          }
      }
    }
    Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { _ in
      Task.detached(priority: .userInitiated) {
        if !chart.isEmpty {
          chart.remove(at: 0)
        }
      }
    }
  }
}





