import SwiftUI

let screenSize: CGRect = UIScreen.main.bounds

var colorDict:[Int:Color] = [:]

// Race Condition fix
actor Counter {
    let identity = UUID()
    var value = 0
    var values:[Int] = []
    
    
    
    func increment() async -> [Int]{
      let newValues = await qincrement()
      return newValues
    }
    
  func qincrement() async -> [Int]{
        self.value += 1
        values.append(value)
        do {
          try await Task.sleep(seconds: 2)
        } catch {
          print("bomb")
        }
        return(values)
    }
    
    nonisolated func returnID() -> UUID {
        return identity
    }
}

struct ContentView: View {
    var counter = Counter()
    
    @State var refresh = 0
    @State var bars:[Int] = []
    @State var offset:CGSize = .zero
    @State var runner = 0
    
    let timer1 = Timer.publish(every: 4, on: .main, in: .common).autoconnect()
    
    var body: some View {
        VStack {
            Color.clear
                .frame(width: 1, height: 1, alignment: .center)
                .onReceive(timer1) { _ in
                  if runner < 256 {
                      Task.detached(priority: .userInitiated) {
                          await bars = counter.increment()
                          runner += 1
                      }
                    }
                }
                .onReceive(timer1) { _ in
                  if runner < 256 {
                    Task.detached(priority: .userInitiated) {
                        await bars = counter.increment()
                        runner += 1
                    }
                  }
                }
                .onReceive(timer1) { _ in
                    refresh += 1
                    print("refresh ",refresh,runner)
                    if refresh > 15 && runner < 256 {
                        offset.width -= 24
                    }
                }
                .id(refresh)
                
            BarView(bars: $bars, multiple: 2)
                .frame(width: screenSize.width - 64, alignment: .center)
                .offset(offset)
        }
    }
}

struct BarView: View {
    let colors = [Color.red, .purple, .yellow, .pink, .green, .teal, .orange, .indigo, .blue]
    @Binding var bars:[Int]
    @State var multiple:Int
    
    var body: some View {
        HStack {
                ForEach((0..<bars.count), id: \.self) { count in
                    VStack {
                        Text(String(bars[count]))
                            .font(.caption).bold()
                            .foregroundColor(Color.gray)
                            .frame(width: 17, alignment: .center)
                        Rectangle()
                            .fill(returnColor(barSize: bars[count]))
                            .frame(width: 18, height: CGFloat(bars[count] * multiple), alignment: .center)
                    }
                }
        }.frame(width: screenSize.width - 64, alignment: .leading)
        
    }
    func returnColor(barSize:Int) -> Color {
        if colorDict[barSize] != nil {
            return colorDict[barSize]!
        } else {
            let colorIndex = barSize % colors.count
            colorDict[barSize] = colors[colorIndex]
            return colors[colorIndex]
        }
    }
}

extension Task where Success == Never, Failure == Never {
  static func sleep(seconds: Int) async throws {
    let duration = UInt64(seconds * 1_000_000_000)
    try await sleep(nanoseconds: duration)
  }
}